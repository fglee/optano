from setuptools import setup, find_packages

from optano import __version__

setup(name='optano',
      version=__version__,
      description='Python library for processing imaging mass spectrometry data',
      url='https://bitbucket.org/elucidatainc/optano',
      author='Francis G. Lee',
      packages=find_packages())
