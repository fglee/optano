# optano

> This repository contains a Python library for processing imaging mass spectrometry data.

## Table of Contents

1.  [Package Features](#package-features)
2.  [Getting Started](#getting-started)
3.  [Converting to and from imzML](#converting-to-and-from-imzML)
4.  [Repository Structure](#repository-structure)

---

## Package Contents

This library is currently includes:

- data container class for a imaging mass spectrum
- tools for normalising imaging mass spectrometry data
- data container class for ion images
- tools for processing ion images
- some preliminary implementations of data converation utilities

---

## Getting Started

### Dependencies

- Ubuntu 16.04 +
- Python3.6 +

### Installation

1.  Clone this repository.

    ```bash
    $ git clone https://www.bitbucket.org/elucidatainc/optano
    ```

2.  Create and activate a Python3 virtual environment.

    ```bash
    $ python3.6 -m .venv venv
    $ source .venv/bin/activate
    (.venv)$
    ```

3.  `pip` install requirements.

    ```bash
    (.venv)$ pip install -r requirements.txt
    ```

### Running the Jupyter Notebooks

1.  Load virtual environment to ipython kernel.

    ```bash
    (.venv)$ python3.6 -m ipykernel install --user --name=.venv
    ```

2.  Start Jupyter Notebook.

    ```bash
    (.venv)$ jupyter notebook
    ```

---

## Converting to and from imzML

### Requirements

- `msConvert GUI` (Proteowizard release 3.0.6938),
- imzmlConverter v.1.1.0

**Notes**: As this multi-step conversion does not always preserve all available imaging MS metadata we recommend first trying a vendor provided solution.

### Steps:

1.  Open msConvert GUI
2.  Add file(s)
    - browse -> select files -> OK
    - `Add`
3.  Output format
    - `mzML`
    - 32-bit (optional, 64 bit also fine)
    - no compression
    - choose output dir
4.  Add filter
    - Select `Peak Picking` from the drop-down and check the `Prefer Vendor` box
    - `Add`
5.  Click `Start`.
6.  Launch imzmlConverter (from run.bat file)
7.  `Add` - select the mzML file you just created
8.  Select `Image per file` from File organisation
9.  Input image dimensions (Pixels in x, Pixels in y)
10. Add additional meta-data using tabs at the top
11. `Convert` -> select file

---

## Repository Architecture

```
optano - main code repository
tests - pytest
notebooks
webapp
docs
scripts

```

## Tasks

preprocessing -
reduction -
representation -
segmentation -
classification
registration
utils - compression - io -

### preprocessing

- recalibration of spectra (is this alignment?)
- baseline correction
- smoothing
- peak detection
- display
- filtering
- deisotoping

### Processing

- Spatial smoothing
- segmentation
- reduction
- metabolite annotation
-

###
